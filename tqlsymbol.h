#ifndef TQLSYMBOL_H
#define TQLSYMBOL_H

#include "tqlstring.h"
#include "tqlconfig.h"

namespace tql {

// prepend and append symbol begin and end to a name string (for example convert "foo" to "`foo`")
template<typename...>
struct ToSymbol;
template<char ...charsName>
struct ToSymbol<String<charsName...>> {
    typedef String<charsName...> name;
    typedef typename Concat<_SymbolBegin,name>::result _temp;
    typedef typename Concat<_temp,_SymbolEnd>::result result;
};

// prepend and append symbol placeholder begin and end to a name string (for example convert "foo" to "#foo#")
template<typename...>
struct ToSymbolPlaceholder;
template<char ...charsName>
struct ToSymbolPlaceholder<String<charsName...>> {
    typedef String<charsName...> name;
    typedef typename Concat<_SymbolPlaceholderBegin,name>::result _temp;
    typedef typename Concat<_temp,_SymbolPlaceholderEnd>::result result;
};

}

#endif // TQLSYMBOL_H
