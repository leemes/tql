#ifndef TQLBACKEND_H
#define TQLBACKEND_H

#include "tqlselect.h"
#include "tqlupdate.h"

#include <string>
#include <cstring>
#include <stdexcept>

namespace tql {

template<typename Policy>
struct Backend
{
    // The result class is extended with iterators:
    template<typename RecordTuple>
    struct Result : Policy::template Result<RecordTuple>
    {
        // Move constructor wrapping the policy type
        Result(typename Policy::template Result<RecordTuple> && wrapped) :
            Policy::template Result<RecordTuple>(wrapped)
        {}
        // Default constructor representing a dummy result
        Result() :
            Policy::template Result<RecordTuple>()
        {}

        struct const_iterator {
            Result r;
            bool isEnd;
            const_iterator & operator ++()                  { r.nextRecord(); return *this; }
            const_iterator operator ++(int)                 { const_iterator old = *this; ++*this; return old; }
            bool operator==(const const_iterator &o) const  { if (o.isEnd) return r.atEnd(); else if(isEnd) return o.r.atEnd(); return false; }
            bool operator!=(const const_iterator &o) const  { return !(*this == o); }
            RecordTuple operator*() const                   { return r.getRecord(); }
        };

        const_iterator begin() const {
            return { *this, false };
        }
        const_iterator end() const {
            return { {}, true };
        }
    };


    // The statement class:
    template<typename RecordTuple, typename BoundTuple>
    class Statement : Policy::template Statement<RecordTuple, BoundTuple>
    {
        friend class Backend::Connection;

        // Move constructor wrapping the policy type
        Statement(typename Policy::template Statement<RecordTuple, BoundTuple> && wrapped) :
            Policy::template Statement<RecordTuple, BoundTuple>(wrapped)
        {}

    public:
        /** Execute the statement, providing the bound values. */
        Result<RecordTuple> execute(const BoundTuple &boundValues) const {
            // Forward to policy, wrap the returned policy type in the wrapper
            return { Policy::template Statement<RecordTuple, BoundTuple>::execute(boundValues) };
        }
    };


    // The connection class is extended with query creation functions:
    class Connection : Policy::Connection
    {
        std::map<std::string, std::string> instanceNames;

    public:
        // Forward constructor
        template<typename ...Args>
        Connection(Args && ...args) :
            Policy::Connection(std::forward<Args>(args)...)
        {}

        // Set the name of a database instance
        void setDatabaseInstanceName(std::string databaseSchemaName, std::string databaseInstanceName) {
            instanceNames[databaseSchemaName] = databaseInstanceName;
        }

        // Retrieve the "actual" query string for a given query string by replacing database schema names with the current database instance names
        std::string actualQueryString(const char *queryString) const {
            std::string actualQueryString;
            // Replace placeholders. According to the default config, they are of the form #MyDatabaseSchema# and will be replaced by `MyDatabaseInstance`
            const char *queryStringEnd = queryString + strlen(queryString);
            actualQueryString.reserve(queryStringEnd - queryString);
            for (const char *substring = queryString; substring < queryStringEnd; ++substring) {
                // Look for placeholder begin character in query string, if not found, reject placeholder
                if (*substring != _SymbolPlaceholderBegin::c_str[0]) {
                    actualQueryString.push_back(*substring);
                    continue;
                }
                // Find placeholder end character in query string
                const char *placeholderBegin = substring + 1;
                const char *placeholderEnd = strchr(placeholderBegin, _SymbolPlaceholderEnd::c_str[0]);
                // No closing "#" found, reject placeholder
                if (placeholderEnd == nullptr) {
                    actualQueryString.push_back(*substring);
                    continue;
                }
                // Construct placeholder string
                std::string placeholder(placeholderBegin, placeholderEnd);
                // Find replacement by searching for the placeholder in the map
                auto it = instanceNames.find(placeholder);
                if (it == std::end(instanceNames)) {
                    // ### TODO: handle this error... ###
                    throw std::runtime_error("Attempt to execute query referring to multi-instance schema, but instance name has not been set.");
                }
                // Append replacement (with symbol begin and end delimiters) and advance input
                actualQueryString.push_back(_SymbolBegin::c_str[0]);
                actualQueryString.append(it->second);
                actualQueryString.push_back(_SymbolEnd::c_str[0]);
                substring = placeholderEnd;
            }
            return actualQueryString;
        }

        template<typename RecordTuple, typename BoundTuple>
        Statement<RecordTuple, BoundTuple> prepare(std::string actualQueryString) const {
            // Forward to policy, wrap the returned policy type in the wrapper
            return { Policy::Connection::template prepare<RecordTuple, BoundTuple>(actualQueryString) };
        }

        // Create a SELECT statement with this connection:
        template<typename ...T, typename ...ES, typename ...UT, typename ...BT>
        SelectQuery<Backend, ExpressionList<Expression<T, ES, UT, BT>...>>
        select(Expression<T, ES, UT, BT> ...expr) const {
            return { this, std::tuple_cat(static_cast<typename BT::asTuple>(expr)...) };
        }

        // Create an UPDATE statement with this connection:
        template<typename ...IntoTables, typename ...T, typename ...ES, typename ...UT, typename ...BT>
        UpdateQuery<Backend, ExpressionList<SetExpression<T, ES, UT, BT>...>, typename TypeListIsEmpty<TypeList<IntoTables...>,void,TypeList<IntoTables...>>::result>
        update(SetExpression<T, ES, UT, BT> ...expr) const {
            return { this, std::tuple_cat(static_cast<typename BT::asTuple>(expr)...) };
        }
    };
};

}

#endif // TQLBACKEND_H

