#ifndef TQLSTRING_H
#define TQLSTRING_H

namespace tql {

template<char ...chars>
struct String {
    static const char c_str[];
};
template<char ...chars>
const char String<chars...>::c_str[] = { chars..., '\0' };


// concatenate two strings
template<typename...>
struct Concat2;
template<char ...charsA, char ...charsB>
struct Concat2<String<charsA...>, String<charsB...>> {
    typedef String<charsA...> a;
    typedef String<charsB...> b;
    typedef String<charsA..., charsB...> result;
};

// concatenate N strings
template<typename...>
struct Concat;
template<>
struct Concat<> {
    typedef String<> result;
};
template<typename Head, typename ...Tail>
struct Concat<Head, Tail...> {
    typedef Head head;
    typedef typename Concat<Tail...>::result tail;
    typedef typename Concat2<head, tail>::result result;
};

// concatenate N strings with a separator in between (separator is the first argument)
template<typename...>
struct ConcatSeparator;
template<typename Sep>
struct ConcatSeparator<Sep> {
    typedef String<> result;
};
template<typename Sep, typename Single>
struct ConcatSeparator<Sep, Single> {
    typedef Single result;
};
template<typename Sep, typename Head, typename ...Tail>
struct ConcatSeparator<Sep, Head, Tail...> {
    typedef Head head;
    typedef typename ConcatSeparator<Sep, Tail...>::result tail;
    typedef typename Concat<head, Sep, tail>::result result;
};


// special case of ConcatSeparator: concatenate N strings with dot ('.') in between
template<typename ...Strings>
using ConcatDot = ConcatSeparator<String<'.'>, Strings...>;

// special case of ConcatSeparator: concatenate N strings with comma and a space (", ") in between
template<typename ...Strings>
using ConcatCommaSpace = ConcatSeparator<String<',',' '>, Strings...>;




// STRING FROM BOOL

template<bool>
struct StringFromBool {
    typedef String<'0'> result;
};
template<>
struct StringFromBool<true> {
    typedef String<'1'> result;
};


// STRING FROM UINT

// fwd-declare the base case:
template<unsigned long long int>
struct StringFromUInt;
// special cases: single digits
#define _TQL_UINT_DIGIT(X) template<> struct StringFromUInt<X> { typedef String<#X[0]> result; };
_TQL_UINT_DIGIT(0)
_TQL_UINT_DIGIT(1)
_TQL_UINT_DIGIT(2)
_TQL_UINT_DIGIT(3)
_TQL_UINT_DIGIT(4)
_TQL_UINT_DIGIT(5)
_TQL_UINT_DIGIT(6)
_TQL_UINT_DIGIT(7)
_TQL_UINT_DIGIT(8)
_TQL_UINT_DIGIT(9)
#undef _TQL_UINT_DIGIT
// base case: concatenate digits
template<unsigned long long int N>
struct StringFromUInt {
    typedef typename Concat<typename StringFromUInt<N/10>::result,
                            typename StringFromUInt<N%10>::result>::result result;
};



// STRING FROM INT

namespace detail {

template<bool>            struct MaybeSign        { typedef String<> result; };
template<>                struct MaybeSign<true>  { typedef String<'-'> result; };
template<long long int N> struct Sign             { typedef typename MaybeSign<(N<0)>::result result; };
template<long long int N> struct Abs              { typedef typename StringFromUInt<(unsigned long long int)((N<0)?-N:N)>::result result; };

}

template<long long int N>
struct StringFromInt {
    typedef typename Concat<typename detail::Sign<N>::result,
                            typename detail::Abs<N>::result>::result result;
};



// STRING FROM ANY TYPE

template<typename T, T value>
struct StringFrom {
    static_assert(value != value, "This type can't be used in a constant expression.");
};

template<char N>
struct StringFrom<char, N> : StringFromInt<N> {};
template<unsigned char N>
struct StringFrom<unsigned char, N> : StringFromUInt<N> {};
template<short N>
struct StringFrom<short, N> : StringFromInt<N> {};
template<unsigned short N>
struct StringFrom<unsigned short, N> : StringFromUInt<N> {};
template<int N>
struct StringFrom<int, N> : StringFromInt<N> {};
template<unsigned int N>
struct StringFrom<unsigned int, N> : StringFromUInt<N> {};
template<long int N>
struct StringFrom<long int, N> : StringFromInt<N> {};
template<unsigned long int N>
struct StringFrom<unsigned long int, N> : StringFromUInt<N> {};
template<long long int N>
struct StringFrom<long long int, N> : StringFromInt<N> {};
template<unsigned long long int N>
struct StringFrom<unsigned long long int, N> : StringFromUInt<N> {};
template<bool B>
struct StringFrom<bool, B> : StringFromBool<B> {};


}

#endif // TQLSTRING_H
