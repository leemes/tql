#ifndef TQLQUERY_H
#define TQLQUERY_H

#include "tqltypelist.h"
#include <map>
#include <tuple>
#include <memory>

namespace tql {

template<typename...>
struct Query;

template<typename Backend, typename RecordTuple, typename BoundTuple, typename QueryString>
struct Query<Backend, RecordTuple, BoundTuple, QueryString> {
    // Accessing the template parameters:
    typedef RecordTuple recordTuple;
    typedef BoundTuple boundTuple;

    // Pulling types from backend:
    typedef typename Backend::Connection Connection;
    typedef typename Backend::template Statement<RecordTuple, BoundTuple> Statement;
    typedef typename Backend::template Result<RecordTuple> Result;
    typedef typename Result::const_iterator const_iterator;

    // Get the query as a C-string:
    static constexpr const char* queryString() { return QueryString::c_str; }

    // Static pool of prepared statements of this query (one statement per connection and set of schema instance names):
    typedef std::map<std::string, Statement> ActualStatements;
    typedef std::map<const Connection*, ActualStatements> StatementPool;
    static StatementPool statementPool;

    // Each query instance knows its connection, the bound values:
    const Connection *connection;
    BoundTuple boundValues;

    Query(const Connection *connection, BoundTuple boundValues) :
        connection(connection), boundValues(boundValues)
    {}

    /** Preparing a query. Returns a prepared statement ready to be executed. */
    const Statement & prepare() const {
        // Construct actual query string (by replacing database schema names into database instance names)
        std::string actualQueryString = connection->actualQueryString(queryString());
        // Find map of "actual statements" for this connection
        auto statementPoolIterator = statementPool.find(connection);
        if (statementPoolIterator == std::end(statementPool)) {
            // Create new map of "actual statements" for this connection
            std::tie(statementPoolIterator, std::ignore) = statementPool.insert(std::make_pair(connection, ActualStatements()));
        }
        // Find prepared statement for this actual query
        auto & actualStatements = statementPoolIterator->second;
        auto actualStatementIterator = actualStatements.find(actualQueryString);
        if (actualStatementIterator == std::end(actualStatements)) {
            // Prepare the statement
            Statement statement = connection->template prepare<RecordTuple, BoundTuple>(actualQueryString);
            std::tie(actualStatementIterator, std::ignore) = actualStatements.insert(std::make_pair(actualQueryString, statement));
        }
        return actualStatementIterator->second;
    }

    /** Execute the query. Shorthand for query.prepare().execute(boundValues) */
    Result execute() const {
        return prepare().execute(boundValues);
    }

    /** Execute and iterate through the results. Shorthand for query.execute(boundValues).begin(). Note that getting the end iterator does not execute the query. */
    const_iterator begin() const {
        return execute().begin();
    }
    const_iterator end() const {
        return { {}, true };
    }
};

// define static member:
template<typename Backend, typename RecordTuple, typename BoundTuple, typename QueryString>
typename Query<Backend, RecordTuple, BoundTuple, QueryString>::StatementPool
Query<Backend, RecordTuple, BoundTuple, QueryString>::statementPool;


}

#endif // TQLQUERY_H
