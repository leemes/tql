#ifndef TQLFIELD_H
#define TQLFIELD_H

#include "tqlstring.h"
#include "tqlsymbol.h"
#include "tqlexpression.h"
#include "tqltypelist.h"

namespace tql {

template<typename Table, typename T, typename NameString,
         typename SymbolString = typename ToSymbol<NameString>::result,
         typename ExpressionString = typename ConcatDot<typename Table::toString, SymbolString>::result>
struct Field : Expression<T, ExpressionString, TypeList<Table>, TypeList<>> {
    typedef Table table;
    typedef typename table::database database;
    typedef NameString name;
    typedef SymbolString symbol;


    // To denote a "SET" clause for this field, i.e. for use in a query where you want to assign the field a value,
    // we write the operator=(Field, X) with X being either an expression or a bound value.
    template<typename ES2, typename TL2, typename BL,
             typename ES = typename Concat<ExpressionString, String<' ','=',' '>, ES2>::result,
             typename TL = typename TypeListUnique<typename TypeListPrepend<Table, TL2>::result>::result>
    SetExpression<T, ES, TL, BL> operator =(Expression<T, ES2, TL2, BL> value) const {
        return { ConstructFromTuple_Tag{}, static_cast<typename BL::asTuple>(value) };
    }
    template<typename T2,
             typename ES = typename Concat<ExpressionString, String<' ','=',' '>, _BoundValuePlaceholder>::result,
             typename TL = TypeList<Table>,
             typename BL = TypeList<decltype(std::cref(std::declval<const T2 &>()))>,
             typename std::enable_if<traits::IsBindable<typename std::decay<T2>::type>::value && std::is_lvalue_reference<T2>::value>::type* = nullptr>
    SetExpression<T, ES, TL, BL> operator =(T2 &&value) const {
        return { ConstructFromTuple_Tag{}, std::make_tuple(std::cref(value)) };
    }
    template<typename T2,
             typename ES = typename Concat<ExpressionString, String<' ','=',' '>, _BoundValuePlaceholder>::result,
             typename TL = TypeList<Table>,
             typename BL = TypeList<decltype(std::cref(std::declval<const T2 &>()))>,
             typename std::enable_if<traits::IsBindable<typename std::decay<T2>::type>::value && !std::is_lvalue_reference<T2>::value>::type* = nullptr>
    SetExpression<T, ES, TL, BL> operator =(T2 &&) const {
        static_assert(!std::is_same<T2,T2>::value, "When using operator= with TQL field, bound value needs to be an lvalue reference. If you tried to bind a constant literal, wrap it in TQL_LITERAL(value).");
        return { ConstructFromTuple_Tag{}, std::tuple<>() };
    }
    template<typename T2,
             typename ES = typename Concat<ExpressionString, String<' ','=',' '>, _BoundValuePlaceholder>::result,
             typename TL = TypeList<Table>,
             typename BL = TypeList<decltype(std::cref(std::declval<const T2 &>()))>,
             typename std::enable_if<!traits::IsBindable<typename std::decay<T2>::type>::value>::type* = nullptr>
    SetExpression<T, ES, TL, BL> operator =(T2 &&) const {
        static_assert(!std::is_same<T2,T2>::value, "When using operator= with TQL field, bound value needs to be compatible with TQL. Please refer to `tql::traits::IsBindable<T>`.");
        return { ConstructFromTuple_Tag{}, std::tuple<>() };
    }
};

}

#endif // TQLFIELD_H
