#ifndef TQLUPDATE_H
#define TQLUPDATE_H

#include "tqlquery.h"
#include "tqlstring.h"
#include "tqlfield.h"
#include "tqlkeywords.h"
#include "tqltypelist.h"
#include "tqlexpression.h"
#include "tqlclauses.h"

namespace tql {

template<typename Backend,
         typename SetExpressions,
         typename ExplicitTarget = void,
         typename Conditions = ExpressionList<>,
         typename RecordTuple = typename SetExpressions::valuesTuple,
         typename BoundTuple = decltype(std::tuple_cat(std::declval<typename SetExpressions::boundValuesTuple>(), std::declval<typename Conditions::boundValuesTuple>())),
         typename UpdateClause = typename detail::UpdateClause<ExplicitTarget, typename SetExpressions::usedTables>::result,
         typename SetClause = typename Concat<String<' '>, _SET, String<' '>, typename SetExpressions::toString>::result,
         typename WhereClause = typename detail::WhereClause<Conditions>::result,
         typename QueryString = typename Concat<UpdateClause, SetClause, WhereClause, String<';'>>::result>
struct UpdateQuery : Query<Backend, std::tuple<>, BoundTuple, QueryString>
{
    // Accessing the template parameters:
    typedef SetExpressions setExpressions;
    typedef Conditions conditions;

    // Constructing from a connection
    UpdateQuery(const typename Backend::Connection *connection, BoundTuple boundValues) :
        Query<Backend, std::tuple<>, BoundTuple, QueryString>(connection, boundValues)
    {}

    // add a condition to the WHERE clause
    template<typename T, typename ES, typename TL, typename BL>
    auto where(Expression<T,ES,TL,BL> condition) const
            -> UpdateQuery<Backend, SetExpressions, ExplicitTarget, typename ExpressionListConcat2<Conditions, ExpressionList<Expression<T,ES,TL,BL>>>::result> {
        return { this->connection, std::tuple_cat(this->boundValues, static_cast<typename BL::asTuple>(condition)) };
    }
    // operator[] is a shorthand for .where
    template<typename Expr>
    auto operator[](Expr e) const -> decltype(this->where(e)) {
        return this->where(e);
    }
};

}

#endif // TQLUPDATE_H
