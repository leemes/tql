#ifndef TQLBACKENDQTSQL_H
#define TQLBACKENDQTSQL_H

#include "tqlbackend.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>

namespace tql {

namespace detail {

// Helper: pack a record as returned by a QSqlQuery into a tuple (convert each element from QVariant to the compile-time type T)
struct TupleFromQSqlQueryRecord {
    // special case: empty list
    template <size_t Index>
    static std::tuple<> get(const QSqlQuery &) {
        return std::tuple<>();
    }
    // general case
    template <size_t Index, typename T, typename ...Tail>
    static std::tuple<T,Tail...> get(const QSqlQuery & q) {
        // Retrieve value as a QVariant, then unpack it as a T
        T value = q.value(Index).value<T>();
        // Recursive call
        auto tailTuple = get<Index + 1, Tail...>(q);
        // Prepend tuple with our fetched value
        return std::tuple_cat(std::make_tuple(value), tailTuple);
    }
};
template<typename ...T>
static std::tuple<T...> tupleFromQSqlQueryRecord(const QSqlQuery &q) {
    return TupleFromQSqlQueryRecord::get<0, T...>(q);
}

// Helper: unpack a tuple in QVariants as needed by QSqlQuery into a tuple (convert each element from QVariant to the compile-time type T)
template <int Index, typename ...Ts>
struct BindTupleToQSqlQuery {
    // general case
    static void bind(QSqlQuery & q, const std::tuple<Ts...> & tup) {
        // Recursive call
        BindTupleToQSqlQuery<Index - 1, Ts...>::bind(q, tup);
        // Get value from tuple and pack it as QVariant
        QVariant variant = QVariant::fromValue(std::get<Index>(tup).get());
        // Bind to QSqlQuery
        q.bindValue(Index, variant);
    }
};
// special case: empty list
template <typename ...Ts>
struct BindTupleToQSqlQuery<-1, Ts...> {
    static void bind(QSqlQuery &, const std::tuple<Ts...> &) {
    }
};
template<typename ...Ts>
static void bindTupleToQSqlQuery(QSqlQuery &q, const std::tuple<Ts...> & tup) {
    return BindTupleToQSqlQuery<int(sizeof...(Ts)) - 1, Ts...>::bind(q, tup);
}

}



struct BackendPolicyQtSql
{
    template<typename RecordTuple>
    class Result;
    template<typename ...RecordTypes>
    class Result<std::tuple<RecordTypes...>>
    {
    public:
        QSqlQuery q;
        bool e;

    public:
        Result(QSqlQuery q) :
            q(q),
            e(!this->q.next())
        {
        }
        Result() :
            q(),
            e(true)
        {
        }

        // For queries which have result rows (SELECT): testing if there are no more records available to be fetched
        bool atEnd() const {
            return e;
        }

        // For queries which have result rows (SELECT): advancing to the next record
        void nextRecord() {
            e = !q.next();
        }

        // For queries which have result rows (SELECT): fetching a record (result row)
        std::tuple<RecordTypes...> getRecord() const {
            return detail::tupleFromQSqlQueryRecord<RecordTypes...>(q);
        }

        // For modifying queries (INSERT, UPDATE, REMOVE): retrieving the number of affected rows
        std::size_t numRowsAffected() const {
            return q.numRowsAffected();
        }

        // For insertion queries (INSERT): last insert ID
        std::size_t lastInsertId() const {
            return q.lastInsertId().toULongLong();
        }
    };

    template<typename RecordTuple, typename BoundTuple>
    class Statement;
    template<typename ...RecordTypes, typename ...BoundTypes>
    class Statement<std::tuple<RecordTypes...>, std::tuple<BoundTypes...>>
    {
        QSqlQuery q;

    public:
        Statement(const QSqlDatabase &db, const char *queryString) :
            q(db)
        {
            qDebug() << "Preparing: " << queryString;
            q.prepare(queryString);
        }

        // Executing the prepared statement by giving the values for the placeholders ("bound values"):
        Result<std::tuple<RecordTypes...>> execute(const std::tuple<BoundTypes...> &boundValues) const {
            QSqlQuery qq(q);
            detail::bindTupleToQSqlQuery(qq, boundValues);
            qq.exec();
            return { qq };
        }
    };

    class Connection
    {
        QSqlDatabase db;

    public:
        // Construct a new connection from a QSqlDatabase object:
        Connection(const QSqlDatabase &db) :
            db(db)
        {}

        // Preparing a statement from a queryString with placeholders (BoundTuple) and some results (RecordTuple), both possibly empty:
        template<typename RecordTuple, typename BoundTuple>
        Statement<RecordTuple, BoundTuple> prepare(const std::string & queryString) const {
            return { db, queryString.c_str() };
        }
    };
};

typedef Backend<BackendPolicyQtSql> BackendQtSql;

}

#endif // TQLBACKENDQTSQL_H
