#ifndef TEST_MYDATABASE_H
#define TEST_MYDATABASE_H

#include "tqlfield.h"
#include "tqlstring.h"
#include "tqlsymbol.h"

#include <QPoint>
#include <QString>

namespace mydb {

struct MyDatabase {
    typedef tql::String<'M','y','D','a','t','a','b','a','s','e'> name;
    typedef tql::ToSymbolPlaceholder<name>::result toString;
};


// The table is represented by a class template, such that we can work around
// the One Definition Rule for our static members. For this, introduce a dummy
// template parameter.
template<typename Dummy = void>
struct TestTable {
    // The database the table belongs to
    typedef MyDatabase database;

    // The name of the table, either as a raw string or as a symbol
    typedef tql::String<'T','e','s','t'> name;
    typedef tql::ToSymbol<name>::result symbol;
    typedef tql::ConcatDot<database::toString,symbol>::result toString;

    // Declare the fields as static constexpr members; their name is reflected
    // in the template parameter for tql::Field.
    static constexpr const tql::Field<TestTable<Dummy>,int,tql::String<'a'>> a = {};
    static constexpr const tql::Field<TestTable<Dummy>,int,tql::String<'b'>> b = {};
};

// Define the static members; multiple definitions are resolved by the linker because the class is a template.
template<typename Dummy>
constexpr const tql::Field<TestTable<Dummy>,int,tql::String<'a'>> TestTable<Dummy>::a;
template<typename Dummy>
constexpr const tql::Field<TestTable<Dummy>,int,tql::String<'b'>> TestTable<Dummy>::b;

// Make the actual type the user should use a non-template type.
typedef TestTable<> Test;


}

#endif // TEST_MYDATABASE_H

