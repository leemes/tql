#ifndef TQLEXPRESSION_H
#define TQLEXPRESSION_H

#include "tqlstring.h"
#include "tqlkeywords.h"
#include "tqltypelist.h"
#include "tqltraits.h"

#include <tuple>


namespace tql {

// Tag type
struct ConstructFromTuple_Tag{
    constexpr explicit ConstructFromTuple_Tag() {}
};

// Generic expression
template<typename...>
struct Expression;
template<typename T, char ...expressionChars, typename ...UsedTables, typename ...BoundTypes>
struct Expression<T, String<expressionChars...>, TypeList<UsedTables...>, TypeList<BoundTypes...>> : std::tuple<BoundTypes...> {
    typedef T value_type;
    typedef TypeList<UsedTables...> usedTables;
    typedef String<expressionChars...> toString;
    typedef std::tuple<BoundTypes...> boundValuesTuple;

    // Constructor
    constexpr Expression(ConstructFromTuple_Tag, std::tuple<BoundTypes...> boundValues) :
        std::tuple<BoundTypes...>(boundValues)
    {}

    // If no bound types are present (tuple is the empty tuple), then we also accept a default constructor:
    constexpr Expression() :
        std::tuple<>()
    {}
};


// Expression representing a literal constant
template<typename T, T value>
using Literal = Expression<T, typename StringFrom<T,value>::result, TypeList<>, TypeList<>>;

// Helper macros which require only naming the actual literal (not the type)
#define TQL_LITERAL_T(value)    tql::Literal<decltype(value),value>
#define TQL_LITERAL(value)      TQL_LITERAL_T(value){}
#define TQL_TRUE_T              TQL_LITERAL_T(true)
#define TQL_TRUE                TQL_LITERAL(true)
#define TQL_FALSE_T             TQL_LITERAL_T(false)
#define TQL_FALSE               TQL_LITERAL(false)
#define TQL_0_T                 TQL_LITERAL_T(0)
#define TQL_0                   TQL_LITERAL(0)
#define TQL_1_T                 TQL_LITERAL_T(1)
#define TQL_1                   TQL_LITERAL(1)



// EXPRESSION OPERATORS

#define _TQL_EXPRESSION_OP1PRE(op, opstr...) \
    template<typename T1, typename ES1, typename TL, typename BL, \
             typename T = decltype(op std::declval<T1>()), \
             typename ES = typename Concat<String<opstr,' '>, ES1>::result> \
    Expression<T, ES, TL, BL> operator op(Expression<T1, ES1, TL, BL> expr) { \
        return { ConstructFromTuple_Tag{}, static_cast<typename BL::asTuple>(expr) }; \
    }

#define _TQL_EXPRESSION_OP2(op, opstr...) \
    template<typename T1, typename ES1, typename TL1, typename BL1, \
             typename T2, typename ES2, typename TL2, typename BL2, \
             typename T = decltype(std::declval<T1>() op std::declval<T2>()), \
             typename ES = typename Concat<String<'('>, ES1, String<' ',opstr,' '>, ES2, String<')'>>::result, \
             typename TL = typename TypeListUnique<typename TypeListConcat2<TL1,TL2>::result>::result, \
             typename BL = typename TypeListConcat2<BL1,BL2>::result> \
    Expression<T, ES, TL, BL> operator op(Expression<T1, ES1, TL1, BL1> lhs, Expression<T2, ES2, TL2, BL2> rhs) { \
        return { ConstructFromTuple_Tag{}, std::tuple_cat(static_cast<typename BL1::asTuple>(lhs), static_cast<typename BL2::asTuple>(rhs)) }; \
    } \
    \
    template<typename T1, typename ES1, typename TL, typename BL1, \
             typename T2, \
             typename T = decltype(std::declval<T1>() op std::declval<T2>()), \
             typename ES = typename Concat<String<'('>, ES1, String<' ',opstr,' '>, _BoundValuePlaceholder, String<')'>>::result, \
             typename BL = typename TypeListConcat2<BL1, TypeList<decltype(std::cref(std::declval<const T2 &>()))>>::result, \
             typename std::enable_if< \
                    !traits::IsField<typename std::decay<T2>::type>::value \
                    && traits::IsBindable<typename std::decay<T2>::type>::value \
                    && std::is_lvalue_reference<T2>::value>::type* = nullptr> \
    Expression<T, ES, TL, BL> operator op(Expression<T1, ES1, TL, BL1> lhs, T2 &&rhs) { \
        return { ConstructFromTuple_Tag{}, std::tuple_cat(static_cast<typename BL1::asTuple>(lhs), std::make_tuple(std::cref(rhs))) }; \
    } \
    \
    template<typename T1, typename ES1, typename TL, typename BL1, \
             typename T2, \
             typename std::enable_if< \
                    !traits::IsField<typename std::decay<T2>::type>::value \
                    && traits::IsBindable<typename std::decay<T2>::type>::value \
                    && !std::is_lvalue_reference<T2>::value>::type* = nullptr> \
    Expression<T1, ES1, TL, BL1> operator op(Expression<T1, ES1, TL, BL1> lhs, T2 &&) { \
        static_assert(!std::is_same<T2,T2>::value, "When using operator" #op " with TQL expression, bound value needs to be an lvalue reference. If you tried to bind a constant literal, wrap it in TQL_LITERAL(value)."); \
        return { ConstructFromTuple_Tag{}, static_cast<typename BL1::asTuple>(lhs) }; \
    } \
    \
    template<typename T1, typename ES1, typename TL, typename BL1, \
             typename T2, \
             typename std::enable_if< \
                    !traits::IsField<typename std::decay<T2>::type>::value \
                    && !traits::IsBindable<typename std::decay<T2>::type>::value>::type* = nullptr> \
    Expression<T1, ES1, TL, BL1> operator op(Expression<T1, ES1, TL, BL1> lhs, T2 &&) { \
        static_assert(!std::is_same<T2,T2>::value, "When using operator" #op " with TQL expression, bound value needs to be compatible with TQL. Please refer to `tql::traits::IsBindable<T>`."); \
        return { ConstructFromTuple_Tag{}, static_cast<typename BL1::asTuple>(lhs) }; \
    } \
    \
    template<typename T1, \
             typename T2, typename ES2, typename TL, typename BL2, \
             typename T = decltype(std::declval<T1>() op std::declval<T2>()), \
             typename ES = typename Concat<String<'('>, _BoundValuePlaceholder, String<' ',opstr,' '>, ES2, String<')'>>::result, \
             typename BL = typename TypeListConcat2<TypeList<decltype(std::cref(std::declval<const T1 &>()))>, BL2>::result, \
             typename std::enable_if< \
                    !traits::IsField<typename std::decay<T1>::type>::value \
                    && traits::IsBindable<typename std::decay<T1>::type>::value \
                    && std::is_lvalue_reference<T1>::value>::type* = nullptr> \
    Expression<T, ES, TL, BL> operator op(T1 &&lhs, Expression<T2, ES2, TL, BL2> rhs) { \
        return { ConstructFromTuple_Tag{}, std::tuple_cat(std::make_tuple(std::cref(lhs)), static_cast<typename BL2::asTuple>(rhs)) }; \
    } \
    \
    template<typename T1, \
             typename T2, typename ES2, typename TL, typename BL2, \
             typename std::enable_if< \
                    !traits::IsField<typename std::decay<T1>::type>::value \
                    && traits::IsBindable<typename std::decay<T1>::type>::value \
                    && !std::is_lvalue_reference<T1>::value>::type* = nullptr> \
    Expression<T2, ES2, TL, BL2> operator op(T1 &&, Expression<T2, ES2, TL, BL2> rhs) { \
        static_assert(!std::is_same<T1,T1>::value, "When using operator" #op " with TQL expression, bound value needs to be an lvalue reference. If you tried to bind a constant literal, wrap it in TQL_LITERAL(value)."); \
        return { ConstructFromTuple_Tag{}, static_cast<typename BL2::asTuple>(rhs) }; \
    } \
    \
    template<typename T1, \
             typename T2, typename ES2, typename TL, typename BL2, \
             typename std::enable_if< \
                    !traits::IsField<typename std::decay<T1>::type>::value \
                    && !traits::IsBindable<typename std::decay<T1>::type>::value>::type* = nullptr> \
    Expression<T2, ES2, TL, BL2> operator op(T1 &&, Expression<T2, ES2, TL, BL2> rhs) { \
        static_assert(!std::is_same<T1,T1>::value, "When using operator" #op " with TQL expression, bound value needs to be compatible with TQL. Please refer to `tql::traits::IsBindable<T>`."); \
        return { ConstructFromTuple_Tag{}, static_cast<typename BL2::asTuple>(rhs) }; \
    }

#define _TQL_EXPRESSION_OP2_str1(op) _TQL_EXPRESSION_OP2(op, #op[0])
#define _TQL_EXPRESSION_OP2_str2(op) _TQL_EXPRESSION_OP2(op, #op[0], #op[1])

_TQL_EXPRESSION_OP1PRE(!, 'N','O','T')

_TQL_EXPRESSION_OP2_str1(+)
_TQL_EXPRESSION_OP2_str1(-)
_TQL_EXPRESSION_OP2_str1(*)
_TQL_EXPRESSION_OP2_str1(/)
_TQL_EXPRESSION_OP2_str1(>)
_TQL_EXPRESSION_OP2_str1(<)
_TQL_EXPRESSION_OP2_str2(>=)
_TQL_EXPRESSION_OP2_str2(<=)
_TQL_EXPRESSION_OP2_str2(!=)
_TQL_EXPRESSION_OP2(==, '=')
_TQL_EXPRESSION_OP2(||, 'O','R')
_TQL_EXPRESSION_OP2(&&, 'A','N','D')

template<typename T1, typename ES1, typename TL, typename BL,
         typename T = bool,
         typename ES = typename ConcatSeparator<String<' '>, ES1, _IS, _NULL>::result>
Expression<T, ES, TL, BL> is_null(const Expression<T1, ES1, TL, BL> &expr) { return { expr.boundValues() }; }




// Expression representing an assignment (does NOT inherit from Expression; only usable in special contexts; e.g. not usable as sub-expression of any operators)
template<typename...>
struct SetExpression;
template<typename T, char ...expressionChars, typename ...UsedTables, typename ...BoundTypes>
struct SetExpression<T, String<expressionChars...>, TypeList<UsedTables...>, TypeList<BoundTypes...>> : std::tuple<BoundTypes...> {
    typedef T value_type;
    typedef TypeList<UsedTables...> usedTables;
    typedef String<expressionChars...> toString;
    typedef std::tuple<BoundTypes...> boundValuesTuple;

    // Constructor
    constexpr SetExpression(ConstructFromTuple_Tag, std::tuple<BoundTypes...> boundValues) :
        std::tuple<BoundTypes...>(boundValues)
    {}

    // If no bound types are present (tuple is the empty tuple), then we also accept a default constructor:
    constexpr SetExpression() :
        std::tuple<>()
    {}
};



// Expression lists (list of Expression or SetExpression template instantiations)
template<typename ...Args>
struct ExpressionList {
    struct head { static_assert(sizeof...(Args) > 0, "Attempt to access the head of an empty list."); };
    struct tail { static_assert(sizeof...(Args) > 0, "Attempt to access the tail of an empty list."); };
    typedef String<> toString;
    typedef TypeList<> usedTables;
    typedef std::tuple<> valuesTuple;
    typedef std::tuple<> boundValuesTuple;
};
template<typename Head, typename ...Tail>
struct ExpressionList<Head, Tail...> {
    typedef Head head;
    typedef ExpressionList<Tail...> tail;

    // Convert the expression list into a string, listing all expressions with a comma in between.
    // Each expression is converted to string using ::toString.
    typedef typename ConcatSeparator<String<',',' '>, typename Head::toString, typename Tail::toString...>::result toString;

    // Get all used tables from all expressions
    typedef typename TypeListUnique<typename TypeListConcat2<typename head::usedTables, typename ExpressionList<Tail...>::usedTables>::result>::result usedTables;

    // Convert the expression list into a tuple, in which each element has the type of the expression
    typedef decltype(std::tuple_cat(std::make_tuple(std::declval<typename head::value_type>()), std::declval<typename tail::valuesTuple>())) valuesTuple;

    // Get the tuple type for holding the bound values
    typedef decltype(std::tuple_cat(std::declval<typename head::boundValuesTuple>(), std::declval<typename tail::boundValuesTuple>())) boundValuesTuple;
};


// Concatenate two expression lists:
template<typename EL1, typename EL2>
struct ExpressionListConcat2;
template<typename ...Expressions1, typename ...Expressions2>
struct ExpressionListConcat2<ExpressionList<Expressions1...>, ExpressionList<Expressions2...>> {
    typedef ExpressionList<Expressions1..., Expressions2...> result;
};

}

#endif // TQLEXPRESSION_H
