#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlResult>
#include <QSqlRecord>
#include <QDebug>

#include "tqlstring.h"
#include "tqlselect.h"
#include "tqlsymbol.h"
#include "tqlexpression.h"
#include "tqloperators.h"

#include "test_mydatabase.h"
using namespace mydb;
using namespace tql;

#include "tqlbackendqtsql.h"


int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    auto qdb = QSqlDatabase::addDatabase("QMYSQL");
    qdb.setHostName("localhost");
    qdb.setUserName("root");
    qdb.setPassword("root");
    bool ok = qdb.open();
    Q_ASSERT(ok);

    //QSqlQuery q;
    //q.prepare("SELECT `MyDatabase`.`Test`.`a` FROM `MyDatabase`.`Test`");

    BackendQtSql::Connection db(qdb);
    db.setDatabaseInstanceName("MyDatabase", "MyDatabase");

    int a = 4;
    int res;
    auto q = db.select(Test::a);
    foreach (std::tie(res), q) {
        qDebug() << res;
    }
    db.update(Test::a = Test::a + Test::b, Test::b = TQL_1 - Test::b)
            .where(Test::a < TQL_LITERAL(20))
            .execute();
    foreach (std::tie(res), q) {
        qDebug() << res;
    }


    return 0;
}
