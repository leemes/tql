#ifndef TQLSELECT_H
#define TQLSELECT_H

#include "tqlquery.h"
#include "tqlstring.h"
#include "tqlfield.h"
#include "tqlkeywords.h"
#include "tqltypelist.h"
#include "tqlexpression.h"
#include "tqlclauses.h"


namespace tql {

template<typename Backend,
         typename Expressions,
         typename ExplicitSource = void,
         typename Conditions = ExpressionList<>,
         typename RecordTuple = typename Expressions::valuesTuple,
         typename BoundTuple = decltype(std::tuple_cat(std::declval<typename Expressions::boundValuesTuple>(), std::declval<typename Conditions::boundValuesTuple>())),
         typename SelectClause = typename Concat<_SELECT, String<' '>, typename Expressions::toString>::result,
         typename FromClause = typename detail::FromClause<ExplicitSource, typename Expressions::usedTables>::result,
         typename WhereClause = typename detail::WhereClause<Conditions>::result,
         typename QueryString = typename Concat<SelectClause, FromClause, WhereClause, String<';'>>::result>
struct SelectQuery : Query<Backend, RecordTuple, BoundTuple, QueryString>
{
    // Accessing the template parameters:
    typedef Expressions expressions;
    typedef Conditions conditions;

    // Constructing from a connection
    SelectQuery(const typename Backend::Connection *connection, BoundTuple boundValues) :
        Query<Backend, RecordTuple, BoundTuple, QueryString>(connection, boundValues)
    {}

    // add a condition to the WHERE clause
    template<typename T, typename ES, typename TL, typename BL>
    auto where(Expression<T,ES,TL,BL> condition) const
            -> SelectQuery<Backend, Expressions, ExplicitSource, typename ExpressionListConcat2<Conditions, ExpressionList<Expression<T,ES,TL,BL>>>::result> {
        return { this->connection, std::tuple_cat(this->boundValues, static_cast<typename BL::asTuple>(condition)) };
    }
    // operator[] is a shorthand for .where
    template<typename Expr>
    auto operator[](Expr e) const -> decltype(this->where(e)) {
        return this->where(e);
    }

    // add an explicit FROM clause, overriding the implicitly added one which lists all tables mentioned in the SELECT expressions
    template<typename ...Tables>
    auto from() const -> SelectQuery<Backend, Expressions, TypeList<Tables...>, Conditions> {
        return { this->connection, this->boundValues };
    }
};

}

#endif // TQLSELECT_H
