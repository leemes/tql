#ifndef TQLCLAUSES_H
#define TQLCLAUSES_H

#include "tqlstring.h"
#include "tqlkeywords.h"
#include "tqltypelist.h"


namespace tql {

namespace detail {


// FROM clause:

template<typename...>
struct FromClause;
// FROM clause from a list of tables (special case: if list is empty, don't output any clause)
template<>
struct FromClause<TypeList<>> {
    typedef String<> result;
};
template<typename ...Tables>
struct FromClause<TypeList<Tables...>> {
    typedef typename Concat<String<' '>, _FROM, String<' '>, typename ConcatCommaSpace<typename Tables::toString...>::result>::result result;
};
// FROM clause constructed implicitly from the mentioned tables in the expressions of the SELECT clause:
template<typename ...ImplicitTables>
struct FromClause<void, TypeList<ImplicitTables...>> {
    // use the implicit tables
    typedef typename FromClause<TypeList<ImplicitTables...>>::result result;
};
// FROM clause constructed explicitly by providing a TypeList as a template parameter:
template<typename ...ExplicitTables, typename ...ImplicitTables>
struct FromClause<TypeList<ExplicitTables...>, TypeList<ImplicitTables...>> {
    // use the explicit tables
    typedef typename FromClause<TypeList<ExplicitTables...>>::result result;
};


// UPDATE clause:

template<typename...>
struct UpdateClause;
// UPDATE clause from a list of tables
template<typename ...Tables>
struct UpdateClause<TypeList<Tables...>> {
    // As UpdateClause is the main clause of UPDATE, the only query it is used in, it makes no sense to accept an empty set of tables.
    static_assert(sizeof...(Tables) > 0, "You tried to construct an UPDATE query which doesn't touch any tables. You need to add some assignment expressions.");
    typedef typename Concat<_UPDATE, String<' '>, typename ConcatCommaSpace<typename Tables::toString...>::result>::result result;
};
// UPDATE clause constructed implicitly from the mentioned tables in its set expressions:
template<typename ...ImplicitTables>
struct UpdateClause<void, TypeList<ImplicitTables...>> {
    // use the implicit tables
    typedef typename UpdateClause<TypeList<ImplicitTables...>>::result result;
};
// UPDATE clause constructed explicitly by providing a TypeList as a template parameter:
template<typename ...ExplicitTables, typename ...ImplicitTables>
struct UpdateClause<TypeList<ExplicitTables...>, TypeList<ImplicitTables...>> {
    // use the explicit tables
    typedef typename UpdateClause<TypeList<ExplicitTables...>>::result result;
};


// WHERE clause:

template<typename...>
struct WhereClause;
// WHERE clause from a list of conditions (special case: if no condition was given, don't output any clause)
template<>
struct WhereClause<ExpressionList<>> {
    typedef String<> result;
};
template<typename ...Conditions>
struct WhereClause<ExpressionList<Conditions...>> {
    typedef typename Concat<String<' '>, _WHERE, String<' '>, typename ConcatSeparator<String<' ','A','N','D',' '>, typename Conditions::toString...>::result>::result result;
};


}

}


#endif // TQLCLAUSES_H
