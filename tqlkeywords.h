#ifndef TQLKEYWORDS_H
#define TQLKEYWORDS_H

#include "tqlstring.h"

namespace tql {

typedef String<'S','E','L','E','C','T'> _SELECT;
typedef String<'U','P','D','A','T','E'> _UPDATE;
typedef String<'F','R','O','M'> _FROM;
typedef String<'W','H','E','R','E'> _WHERE;
typedef String<'S','E','T'> _SET;
typedef String<'I','S'> _IS;
typedef String<'N','U','L','L'> _NULL;

}

#endif // TQLKEYWORDS_H
