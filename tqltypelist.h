#ifndef TQLTYPELIST_H
#define TQLTYPELIST_H

#include <tuple>

namespace tql {

// Empty type list
template<typename ...Args>
struct TypeList {
    // accessing head or tail of an empty list should result in an error:
    struct head { static_assert(sizeof...(Args) > 0, "Attempt to access the head of an empty TypeList."); };
    struct tail { static_assert(sizeof...(Args) > 0, "Attempt to access the tail of an empty TypeList."); };

    // number of elements in list:
    enum { count = 0 };
    static constexpr int size() { return count; }

    // convert to std::tuple:
    typedef std::tuple<Args...> asTuple;
};

// Non-empty type list
template<typename Head, typename ...Tail>
struct TypeList<Head, Tail...> {
    // accessing head and tail:
    typedef Head head;
    typedef TypeList<Tail...> tail;

    // number of elements in list:
    enum { count = 1 + sizeof...(Tail) };
    static constexpr int size() { return count; }

    // convert to std::tuple:
    typedef std::tuple<Head, Tail...> asTuple;
};


// Prepend a new head in front of a type list ("list constructor"):
template<typename...>
struct TypeListPrepend;
template<typename Element, typename ...Types>
struct TypeListPrepend<Element, TypeList<Types...>> {
    typedef TypeList<Element, Types...> result;
};


// Concatenate two type lists:
template<typename TL1, typename TL2>
struct TypeListConcat2;
template<typename ...Types1, typename ...Types2>
struct TypeListConcat2<TypeList<Types1...>, TypeList<Types2...>> {
    typedef TypeList<Types1..., Types2...> result;
};


// Remove the first occurrence of the given element from the type list
template<typename...>
struct TypeListRemove;
// case 1: empty list => result is empty list
template<typename Element>
struct TypeListRemove<Element, TypeList<>> {
    typedef TypeList<> result;
};
// case 2: the head is the element to be removed => result is tail
template<typename Element, typename ...Tail>
struct TypeListRemove<Element, TypeList<Element, Tail...>> {
    typedef TypeList<Tail...> result;
};
// case 3: the head is a different type => recursively apply to tail
template<typename Element, typename Head, typename ...Tail>
struct TypeListRemove<Element, TypeList<Head, Tail...>> {
    typedef typename TypeListPrepend<Head, typename TypeListRemove<Element, TypeList<Tail...>>::result>::result result;
};


// Make a type list unique
template<typename...>
struct TypeListUnique;
template<>
struct TypeListUnique<TypeList<>> {
    typedef TypeList<> result;
};
template<typename Head, typename ...Tail>
struct TypeListUnique<TypeList<Head,Tail...>> {
    typedef typename TypeListUnique<TypeList<Tail...>>::result uniqueTail;
    typedef typename TypeListPrepend<Head, typename TypeListRemove<Head,uniqueTail>::result>::result result;
};


// Check if type list is empty with a ternary operator style.
// Select first type if true, second type if empty.
template<typename...>
struct TypeListIsEmpty;
template<typename TTrue, typename TFalse>
struct TypeListIsEmpty<TypeList<>, TTrue, TFalse> {
    typedef TTrue result;
};
template<typename Head, typename ...Tail, typename TTrue, typename TFalse>
struct TypeListIsEmpty<TypeList<Head,Tail...>, TTrue, TFalse> {
    typedef TFalse result;
};

}

#endif // TQLTYPELIST_H
