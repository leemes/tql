#-------------------------------------------------
#
# Project created by QtCreator 2013-07-03T00:07:31
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = tql
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    anotherfile.cpp

HEADERS += \
    tqlkeywords.h \
    tqlfield.h \
    tqlstring.h \
    tqlselect.h \
    tqlconfig.h \
    tqlsymbol.h \
    tqlexpression.h \
    test_mydatabase.h \
    tqltypelist.h \
    tqloperators.h \
    tqlquery.h \
    tqlbackendqtsql.h \
    tqlbackend.h \
    tqltraits.h \
    tqlupdate.h \
    tqlclauses.h
