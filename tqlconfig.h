#ifndef TQLCONFIG_H
#define TQLCONFIG_H

#include "tqlstring.h"

namespace tql {

typedef String<'`'> _SymbolBegin;               // < Only one character allowed.
typedef String<'`'> _SymbolEnd;                 // < Only one character allowed.
typedef String<'#'> _SymbolPlaceholderBegin;    // < Only one character allowed.
typedef String<'#'> _SymbolPlaceholderEnd;      // < Only one character allowed.
typedef String<'?'> _BoundValuePlaceholder;     // < Any number of characters allowed.

}

#endif // TQLCONFIG_H
