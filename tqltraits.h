#ifndef TQLTRAITS_H
#define TQLTRAITS_H

#include <type_traits>

namespace tql {

// forward-declare:
template<typename T1, typename T2, typename T3, typename T4, typename T5>
struct Field;


namespace traits {

template<typename T>
struct IsBindable : std::integral_constant<bool,
        std::is_integral<T>::value>
{};


template<typename T>
struct IsField : std::false_type
{};
template<typename ...T>
struct IsField<Field<T...>> : std::true_type
{};

}

}

#endif // TQLTRAITS_H
